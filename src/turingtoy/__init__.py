from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    index = 0
    num_steps = 0
    blank = machine["blank"]
    table = machine["table"]
    cur_state = machine["start state"]
    band = list(input_)
    execution_history = []
    accepted = False

    #gaaaa
    while steps is None or num_steps <= steps:
        instructions = table[cur_state][band[index]]
        if isinstance(instructions, str):
            instructions = [instructions]

        execution_history.append({
            "state": cur_state,
            "reading": band[index],
            "position": index,
            "memory": "".join(band),
            "transition": instructions
        })

        for instr in instructions:
            if instr == "write":
                band[index] = instructions["write"]
            elif instr == "R":
                index += 1
                if index == len(band):
                    band.append(blank)
                if isinstance(instructions, dict):
                    cur_state = instructions["R"]
            elif instr == "L":
                if index == 0:
                    band.insert(0, blank)
                else:
                    index -= 1
                if isinstance(instructions, dict):
                    cur_state = instructions["L"]

        num_steps += 1

        if cur_state in machine["final states"]:
            accepted = True
            break

    output = "".join(band).strip(blank)
    return (output, execution_history, accepted)
